## Hello world

Yes, like any other programming language let's write a simple hello world.

As it is our first playbooks let's have a look to this `yaml` file `helloworld.yml`{{open}} :

So a playbook is an array of objects that contains :

* `hosts` on which it applies. It could be machine name, ip address or group name.
* `tasks` which is an array of individual task which execute command using a module
* `debug` is a module which is used to print logs during execution. Here we are using `msg` parameter

Let's play it now :


`ansible-playbook helloworld.yml`{{copy}}




