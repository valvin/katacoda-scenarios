#!/bin/bash
HISTFILE=~/.bash_history
set -o history
history
CMD_TO_CHECK="ansible-playbook hlowld.yml"
PLAYBOOK_HELLOWORLD=`history|grep "$CMD_TO_CHECK"| wc -l`
echo "Command has been run ${PLAYBOOK_HELLOWORLD} times"
if [ $PLAYBOOK_HELLOWORLD -ge 1 ]; then 
  echo "OK"
  exit 0 
else 
  echo "KO"
  exit -1 
fi

